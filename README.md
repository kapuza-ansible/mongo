# Mongo role
## Links
* [MongoDB Download Center](https://www.mongodb.com/download-center/community)
* [Install MongoDB Community Edition on Ubuntu](https://docs.mongodb.com/manual/tutorial/install-mongodb-on-ubuntu/)
* [wiki.epta.suka.se - MongoDB](https://wiki.epta.suka.se/index.php?title=Kon:MongoDB)
* [Enable Access Control](https://docs.mongodb.com/manual/tutorial/enable-authentication/)
* [Configuration File Options](https://docs.mongodb.com/manual/reference/configuration-options/)
* [db.getUsers()](https://docs.mongodb.com/manual/reference/method/db.getUsers/)
* [Replication](https://docs.mongodb.com/manual/replication/)
* [rs.slaveOk()](https://docs.mongodb.com/manual/reference/method/rs.slaveOk/)
* [MongoDB - Not Authorized to add replica set member, but Mongo Auth is disabled](https://stackoverflow.com/questions/50376469/mongodb-not-authorized-to-add-replica-set-member-but-mongo-auth-is-disabled?rq=1)

## Install
### Install python (ubuntu)
For use ansible, you need install python2.7 on remote server.
```bash
ansible mongo -m raw -a "apt-get -qq update;\
apt-get -y --no-install-recommends install python2.7 python-apt"
```

### ansible.cfg example
```bash
cat <<'EOF' > ansible.cfg
[defaults]
hostfile = hosts
remote_user = root
deprecation_warnings = False
roles_path = roles
force_color = 1
retry_files_enabled = False
executable = /bin/bash

[ssh_connection]
ssh_args = -o ControlMaster=auto -o ControlPersist=60s -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no
pipelining = True
EOF
```

### download mongo role
```bash
mkdir ./roles
cat <<'EOF' >> ./roles/req.yml

- src: git+https://gitlab.com/kapuza-ansible/mongo.git
  name: mongo
EOF
echo "roles/mongo" >> .gitignore
ansible-galaxy install --force -r ./roles/req.yml
```

## Mongo (ubuntu)
Install and setup mongo server.
```bash
cat << 'EOF' > mongo.yml
---
- hosts:
    mongo
  roles:
    - role: mongo
EOF

ansible-playbook ./mongo.yml
```
